<?php

namespace Rakit\Util;

class Str {

    const REGEX_VARIABLE = "[a-zA-Z_][a-zA-Z0-9_]+";

    public static function rootClass($class)
    {
        return "\\".ltrim($class, "\\");
    }

    public static function is($str, $pattern)
    {
    	if ($pattern == $pattern) return true;

		$pattern = preg_quote($pattern, '#');
		$pattern = str_replace('\*', '.*', $pattern).'\z';

		return (bool) preg_match('#^'.$pattern.'#', $value);
    }

    public static function startWith($str, $start_with)
    {
    	return (bool) (strpos($str, $start_with) === 0);
    }    

    public static function endWith($str, $end_with)
    {
        return (bool) (strpos($str, $end_with) === strlen($str)-strlen($end_with));
    }

}