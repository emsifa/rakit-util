<?php

/**
 * Utility Class
 * -------------
 * some code here are copy from another open source script that described credits below
 * credits:
 * + Laravel helpers: https://github.com/illuminate/support/blob/master/helpers.php
 */

namespace Rakit\Util;

use Closure;
use Rakit\Util\Arr;
use Rakit\Util\Str;

class Util {

    public static function arraySet(array &$array, $key, $value)
    {
        Arr::set($array, $key, $value);
    }
    
    public static function arrayRemove(array &$array, $key)
    {
        Arr::remove($array, $key);
    }
    
    public static function arrayGet(array $array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
    
    public static function arrayHas(array $array, $key)
    {
        return Arr::has($array, $key);
    }
    
    public static function arrayFlatten(array $array)
    {
        return Arr::flatten($array);
    }
    
    public static function arrayPluck(array $array, $value, $key = null)
    {
        return Arr::pluck($array, $value, $key);
    }
    
    public static function arrayExcept(array $array, array $keys)
    {
        return Arr::except($array, $keys);
    }
    
    public static function arrayOnly(array $array, array $keys)
    {
        return Arr::only($array, $keys);
    }
    
    public static function arrayFetch(array $array, $key)
    {
        return Arr::fetch($array, $key);
    }
    
    public static function arrayPull(&$array, $key)
    {
        return Arr::pull($array, $key);
    }

    public static function arrayWhere(array $array, Closure $finder)
    {
        return Arr::where($array, $finder);
    }

    public static function rootClass($classname)
    {
        return Str::rootClass($classname);
    }

}
