<?php

/**
 * Utility Class
 * -------------
 * some code here are copy from another open source script that described credits below
 * credits:
 * + Laravel helpers: https://github.com/illuminate/support/blob/master/helpers.php
 */

namespace Rakit\Util;

use Closure;

class Arr {

    public static function set(array &$array, $key, $value)
    {
        if(is_null($key)) return $array = $value;
        $keys = explode('.', $key);

        while(count($keys) > 1) {
			$key = array_shift($keys);

			if(!isset($array[$key]) OR !is_array($array[$key])) {
                $array[$key] = array();
            }

            $array =& $array[$key];
        }

        $array[array_shift($keys)] = $value;
    }
    
    public static function remove(array &$array, $key)
    {
        $keys = explode('.', $key);
        
        while(count($keys) > 1) {
            $key = array_shift($keys);

            if(!isset($array[$key]) OR !is_array($array[$key])) {
                $array[$key] = array();
            }

            $array =& $array[$key];
        }

        unset($array[array_shift($keys)]);
    }
    
    public static function get(array $array, $key, $default = null)
    {
        $value = $array;
        $keys = explode('.', $key);
		
        foreach($keys as $i => $path) {
            if(is_array($value)) {
                if(!array_key_exists($path, $value)) {
                    return $default;
                } else {
                    $value = $value[$path];
                }
            } else {
                return $default;   
            }
        }

        return $value;
    }
    
    public static function has(array $array, $key)
    {
        $keys = explode('.', $key);
        $value = $array;
		
        foreach($keys as $i => $path) {
            if(is_array($value)) {
                if(!array_key_exists($path, $value)) {
                    return false;
                } else {
                    $value = $value[$path];
                }
            } else {
                return false;
            }
        }
        
        return true;
    }
    
    public static function flatten(array $array)
    {
        $return = array();
        array_walk_recursive($array, function($x) use (&$return) { $return[] = $x; });
		
        return $return;
    }
    
    public static function pluck(array $array, $value, $key = null)
    {
        $results = array();

        foreach ($array as $item) {
            $itemValue = is_object($item) ? $item->{$value} : $item[$value];

            if (is_null($key)) {
                $results[] = $itemValue;
            } else {
                $itemKey = is_object($item) ? $item->{$key} : $item[$key];
                $results[$itemKey] = $itemValue;
            }
        }

        return $results;
    }
    
    public static function except(array $array, array $keys)
    {
        return array_diff_key($array, array_flip((array) $keys));
    }
    
    public static function only(array $array, array $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
    
    public static function fetch(array $array, $key)
    {
        foreach (explode('.', $key) as $segment) {
            $results = array();

            foreach ($array as $value) {
                $value = (array) $value;
                $results[] = $value[$segment];
            }

            $array = array_values($results);
        }

        return array_values($results);
    }
    
    public static function pull(&$array, $key)
    {
        $value = static::get($array, $key);
        static::remove($array, $key);
    
        return $value;
    }

    public static function where(array $array, $finder)
    {	
        $result = array();

        foreach($array as $key => $value) {
            if(call_user_func($finder, $key, $value)) {
                $result[$key] = $value;
            }
        }
        
        return $result;
    }

}
