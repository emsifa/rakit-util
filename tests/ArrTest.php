<?php

use Rakit\Util\Arr;

class ArrTest extends PHPUnit_Framework_TestCase {

    public function testSet()
    {
        $arr = array();
        Arr::set($arr, "foo", "foo");

        $this->assertEquals($arr['foo'], "foo");

        Arr::set($arr, "foo.bar.baz", "foobarbaz");
        $this->assertEquals($arr['foo']['bar']['baz'], "foobarbaz");
    }

    public function testGet()
    {
        $arr = array(
            'foo' => array(
                'bar' => array(
                    'baz' => 'foobarbaz'
                )
            )
        );

        $this->assertEquals("foobarbaz", Arr::get($arr, "foo.bar.baz"));
        $this->assertEquals(null, Arr::get($arr, "key.not.exists"));
        $this->assertEquals("I AM NOT EXISTS", Arr::get($arr, "key.not.exists", "I AM NOT EXISTS"));
    }

    public function testHas()
    {
        $arr = array(
            'foo' => array(
                'bar' => array(
                    'baz' => 'foobarbaz'
                )
            )
        );

        $this->assertTrue(Arr::has($arr, "foo.bar.baz"));
        $this->assertFalse(Arr::has($arr, "key.not.exists"));
    }

    public function testRemove()
    {
        $arr = array(
            'foo' => array(
                'bar' => array(
                    'baz' => 'foobarbaz'
                )
            )
        );

        Arr::remove($arr, "foo.bar.baz");
        $this->assertFalse( isset($arr['foo']['bar']['baz']) );

        Arr::remove($arr, "foo.bar");
        $this->assertEquals(count($arr['foo']), 0);
    }

    public function testFlatten()
    {
        $arr = array(
            'type' => 'fruit',
            'examples' => array(
                'apple', 'orange', 'mango'
            )
        );

        $arr_flatten = Arr::flatten($arr);

        $this->assertEquals(array('fruit', 'apple', 'orange', 'mango'), $arr_flatten);
    }

    public function testPluck()
    {
        $arr = array(
            array(
                'fruit_name' => 'orange'
            ),
            array(
                'fruit_name' => 'apple'
            )
        );

        $arr_pluck = Arr::pluck($arr, 'fruit_name');

        $this->assertEquals(array('orange', 'apple'), $arr_pluck);
    }

    public function testOnly()
    {
        $arr = array(
            'foo' => 'foo',
            'bar' => 'bar',
            'baz' => 'baz'
        );

        $arr_only = Arr::only($arr, ['foo', 'baz']);

        $this->assertTrue( !isset($arr_only['bar']) AND $arr_only['foo'] == 'foo' AND count($arr_only) == 2 );
    }

    public function testExcept()
    {
        $arr = array(
            'foo' => 'foo',
            'bar' => 'bar',
            'baz' => 'baz'
        );

        $arr_except = Arr::except($arr, ['bar']);

        $this->assertTrue( !isset($arr_except['bar']) AND $arr_except['foo'] == 'foo' AND count($arr_except) == 2 );
    }

    public function testFetch()
    {
        $arr = array(
            array('fruit' => array('name' => 'Orange')),
            array('fruit' => array('name' => 'Apple')),
        );

        $arr_fetch = Arr::fetch($arr, 'fruit.name');

        $this->assertEquals(array('Orange', 'Apple'), $arr_fetch);
    }

    public function testPull()
    {
        $arr = array(
            'foo' => 'foo',
            'bar' => 'bar',
            'baz' => 'baz'
        );

        $bar = Arr::pull($arr, 'bar');

        $this->assertTrue( "bar" == $bar AND !isset($arr['bar']) );
    }

    public function testWhere()
    {
        $arr = array('foo', 1, 'bar', 2, 'baz');

        $arr_where = Arr::where($arr, function($key, $val) {
            return is_string($val);
        });

        $this->assertEquals( array(0,2,4), array_keys($arr_where) );
    }

}